#ifndef ELFLOAD_H
#define ELFLOAD_H

#include <sys/types.h>

#include "elf/elf_generic.h"

struct ELF_File;
typedef void * (*mapping_function)(const char *, struct ELF_File *);

struct hostlib {
    const char *soname;
    mapping_function sym;
};

/* Basic structure for ELF files mid-load */
struct ELF_File {
    char *nm;

    /* if this is a host library, this will be non-NULL. */
    mapping_function hostlib;

    /* the complete program, in memory */
    void *prog;
    size_t proglen;

    /* same pointer, actually */
    Elf_Ehdr *ehdr;

    /* the size in memory of this file */
    ssize_t memsz;

    /* the minimum and maximum position of the loaded file, ideally */
    void *min, *max;

    /* the actual location where this file was loaded */
    void *loc;

    /* the offset of this file's real loaded location from its internal location */
    ssize_t offset;

    /* the dynamic entries table */
    Elf_Dyn *dynamic;

    /* the string table */
    char *strtab;

    /* and symbol table */
    Elf_Sym *symtab;

    /* with its associated hash table */
    Elf_Word *hashtab;
#define ELFFILE_NBUCKET(f) ((f)->hashtab[0])
#define ELFFILE_NCHAIN(f) ((f)->hashtab[1])
#define ELFFILE_BUCKET(f, i) ((f)->hashtab[(i) + 2])
#define ELFFILE_CHAIN(f, i) ((f)->hashtab[(i) + ELFFILE_NBUCKET(f) + 2])

    /* relocation table(s) */
    REL_TYPE *rel;
    size_t relsz;
    size_t relent;
    REL_TYPE *jmprel;
    size_t jmprelsz; // Size in bytes.

    /* Init functions. */
    void (*init_func)(void);
    void (**init_array)(void);
    void (**preinit_array)(void);
    size_t init_arraysz; // Number of entries.
    size_t preinit_arraysz; // Number of entries.
};

void register_hostlib(const char *soname, mapping_function func);

struct ELF_File *loadELF(const char *nm, const char *instdir, int maybe);
void initELF(struct ELF_File *f); // Init a single elf object. Don't mix w/ below.
void initELFs(struct ELF_File *except); // Init ALL loaded elf objects.

#endif
