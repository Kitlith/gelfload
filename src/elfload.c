#ifndef _GNU_SOURCE /* for RTLD_DEFAULT */
#define _GNU_SOURCE 1
#endif

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "memory.h"

#include "../config.h"

#ifdef HAVE_MMAP
#include <sys/mman.h>
#endif

#ifdef HAVE_DLFCN_H
#include <dlfcn.h>
#endif

#ifdef __WIN32
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <windows.h>
#endif

#include "internal.h"
#include "elfload_dlfcn.h"

// TODO: Make these data structures better.
/* An array of files currently in the process of loading */
#define MAX_ELF_FILES 0xFF // Arbitrary
struct ELF_File elfFiles[MAX_ELF_FILES];
int elfFileCount = 0;

#define MAX_HOST_LIBS 0x10 // Arbitrary.
struct hostlib hostlibs[MAX_HOST_LIBS] = {
    { .soname = "libloader_dl.0", .sym = elfload_dl }
};
int hostlib_count = 1;

typedef void (*init_func)(void);

void register_hostlib(const char *soname, mapping_function func) {
    hostlibs[hostlib_count].soname = soname;
    hostlibs[hostlib_count].sym = func;
    ++hostlib_count;
}

void *elfload_hostlib_sym(const char *nm, struct ELF_File *f);
void relocateELF(struct ELF_File *f);

/* The function to actually load ELF files into memory */
// TODO: Remove the "maybe" parameter.
struct ELF_File *loadELF(const char *nm, const char *instdir, int maybe)
{
    int i, fileNo, phdri;
    struct ELF_File *f;
    void *curphdrl;
    Elf_Phdr *curphdr;
    Elf_Dyn *curdyn;

    /* first, make sure it's not already loaded or loading */
    for (i = 0; i < elfFileCount; i++) {
        if (strcmp(elfFiles[i].nm, nm) == 0) return &(elfFiles[i]);
    }

    /* now start preparing to load it */
    fileNo = elfFileCount;
    f = &(elfFiles[fileNo]);
    memset(f, 0, sizeof(struct ELF_File));
    elfFileCount++;
    f->nm = strdup(nm);

    for (i = 0; i < hostlib_count; ++i) {
        if (strcmp(nm, hostlibs[i].soname) == 0) {
            f->hostlib = hostlibs[i].sym;
            return f;
        }
    }

    /* if this is a host library, circumvent all the ELF stuff and go straight for the host */
    if (strncmp(nm, "libhost_", 8) == 0) {
        // f->hostlib = HOSTLIB_HOST;
        f->hostlib = elfload_hostlib_sym;
#if defined(HAVE_DLFCN_H)
        if (strcmp(nm, "libhost_.so") == 0) {
            /* the entire host */
#ifdef RTLD_DEFAULT
            f->prog = RTLD_DEFAULT;
#else
            f->prog = dlopen(NULL, RTLD_NOW|RTLD_GLOBAL);
#endif
        } else {
            f->prog = dlopen(nm + 8, RTLD_NOW|RTLD_GLOBAL);

            if (f->prog == NULL) {
                /* try with an explicit path */
                char *fullpath;
                fullpath = (char *)gelfload_malloc(strlen(elfload_dlinstdir) + strlen(nm) + 1);
                if (fullpath == NULL) {
                    perror("malloc");
                    exit(1);
                }
                sprintf(fullpath, "%s/../lib/%s", elfload_dlinstdir, nm + 8);
                f->prog = dlopen(fullpath, RTLD_NOW|RTLD_GLOBAL);
                gelfload_free(fullpath);
            }

            if (f->prog == NULL) {
                fprintf(stderr, "Could not resolve host library %s: %s.\n", nm + 8, dlerror());
                exit(1);
            }
        }
#elif defined(__WIN32)
        if (strcmp(nm, "libhost_.so") == 0) {
            f->prog = LoadLibrary("msvcrt.dll");
        } else {
            f->prog = LoadLibrary(nm + 8);
        }
        if (f->prog == NULL) {
            fprintf(stderr, "Could not resolve host library %s.\n", nm + 8);
            exit(1);
        }
#else
        fprintf(stderr, "This version of elfload is not capable of loading the host library %s.\n",
                nm + 8);
        exit(1);
#endif
        return f;

    }

    readFile(nm, instdir, f);

    /* make sure it's an ELF file */
    f->ehdr = (Elf_Ehdr *) f->prog;
    if (memcmp(f->ehdr->e_ident, ELFMAG, SELFMAG) != 0) {
        if (!maybe) {
            fprintf(stderr, "%s does not appear to be an ELF file.\n", nm);
            exit(1);
        } else {
            --elfFileCount;
            return NULL;
        }
    }

    /* only native-bit supported for the moment */
    if ((SIZEOF_VOID_P == 4 && f->ehdr->e_ident[EI_CLASS] != ELFCLASS32) ||
        (SIZEOF_VOID_P == 8 && f->ehdr->e_ident[EI_CLASS] != ELFCLASS64)) {
        if (!maybe) {
            fprintf(stderr, "%s is not a %d-bit ELF file.\n", nm, SIZEOF_VOID_P * 8);
            exit(1);
        } else {
            --elfFileCount;
            return NULL;
        }
    }

    /* FIXME: check endianness */

    /* must be an executable or .so to be loaded */
    if (f->ehdr->e_type != ET_EXEC &&
        f->ehdr->e_type != ET_DYN) {
        if (!maybe) {
            fprintf(stderr, "%s is not an executable or shared object file.\n", nm);
            exit(1);
        } else {
            --elfFileCount;
            return NULL;
        }
    }

    /* now go through program headers, to find the allocation space of this file */
    f->min = (void *) -1;
    f->max = 0;
    curphdrl = (void *)((uintptr_t)f->prog + f->ehdr->e_phoff - f->ehdr->e_phentsize);

    for (phdri = 0; phdri < f->ehdr->e_phnum; phdri++) {
        curphdrl = (void *)((uintptr_t)curphdrl + f->ehdr->e_phentsize);
        curphdr = (Elf_Phdr *) curphdrl;

        /* perhaps check its location */
        if (curphdr->p_type == PT_LOAD) {
            /* adjust min/max */
            if ((void *)curphdr->p_vaddr < f->min) {
                f->min = (void *) curphdr->p_vaddr;
            }
            if ((void *)(curphdr->p_vaddr + curphdr->p_memsz) > f->max) {
                f->max = (void *)(curphdr->p_vaddr + curphdr->p_memsz);
            }

        } else if (maybe && curphdr->p_type == PT_INTERP) {
            /* if we're only maybe-loading, check the loader */
            if (strcmp((char *)((uintptr_t)f->prog + curphdr->p_offset), "/usr/bin/gelfload-ld")) {
                /* wrong loader! */
                --elfFileCount;
                return NULL;
            }

        }
    }

    /* with this size info, we can allocate the space */
    f->memsz = (uintptr_t)f->max - (uintptr_t)f->min;

    /* if this is a binary, try to allocate it in place. elfload is addressed above 0x18000000 */
    // TODO: change the hardcoded address to be more portable.
    if (f->ehdr->e_type == ET_EXEC && f->max < (void *) 0x18000000) {
        f->loc = gelfload_pagealloc(f->min, f->memsz);

    } else {
        f->loc = gelfload_pagealloc(NULL, f->memsz);

    }
    if (!f->loc) {
        --elfFileCount;
        return NULL;
    }
    memset(f->loc, 0, f->memsz);

    f->offset = (uintptr_t)f->loc - (uintptr_t)f->min;

    /* we have the space, so load it in */
    curphdrl = (void *)((uintptr_t)f->prog + f->ehdr->e_phoff - f->ehdr->e_phentsize);
    for (phdri = 0; phdri < f->ehdr->e_phnum; phdri++) {
        curphdrl = (void *)((uintptr_t)curphdrl + f->ehdr->e_phentsize);
        curphdr = (Elf_Phdr *) curphdrl;

        /* perhaps load it in */
        if (curphdr->p_type == PT_LOAD) {
            if (curphdr->p_filesz > 0) {
                /* OK, there's something to copy in, so do so */
                memcpy((void *)(curphdr->p_vaddr + f->offset),
                       (void *)((uintptr_t)f->prog + curphdr->p_offset),
                       curphdr->p_filesz);
            }

        } else if (curphdr->p_type == PT_DYNAMIC) {
            /* we need this to load in dependencies, et cetera */
            f->dynamic = (Elf_Dyn *)((uintptr_t)f->prog + curphdr->p_offset);

        }
    }

    // TODO: sanity checking -- what if PT_DYNAMIC or PT_LOAD don't exist?

    /* now go through dynamic entries, looking for basic vital info
     * as well as initialization info for the future. */
    for (curdyn = f->dynamic; curdyn && curdyn->d_tag != DT_NULL; curdyn++) {
        switch (curdyn->d_tag) {
            case DT_STRTAB:
                f->strtab = (char *) (curdyn->d_un.d_ptr + f->offset);
                break;
            case DT_SYMTAB:
                f->symtab = (Elf_Sym *) (curdyn->d_un.d_ptr + f->offset);
                break;
            case DT_HASH:
                f->hashtab = (Elf_Word *) (curdyn->d_un.d_ptr + f->offset);
                break;
#ifdef USE_RELA
            case DT_RELA:
                f->rel = (REL_TYPE *)(curdyn->d_un.d_ptr + f->offset);
                break;
            case DT_RELASZ:
                f->relsz = curdyn->d_un.d_val;
                break;
            case DT_RELAENT:
                f->relent = curdyn->d_un.d_val;
                break;
#else
            case DT_REL:
                f->rel = (REL_TYPE *) (curdyn->d_un.d_ptr + f->offset);
                break;
            case DT_RELSZ:
                f->relsz = curdyn->d_un.d_val;
                break;
            case DT_RELENT:
                f->relent = curdyn->d_un.d_val;
                break;
#endif
            case DT_JMPREL:
                f->jmprel = (REL_TYPE *) (curdyn->d_un.d_ptr + f->offset);
                break;
            case DT_PLTRELSZ:
                f->jmprelsz = curdyn->d_un.d_val;
                break;
            case DT_PREINIT_ARRAY:
                f->preinit_array = (init_func*)(curdyn->d_un.d_ptr + f->offset);
                break;
            case DT_PREINIT_ARRAYSZ:
                f->preinit_arraysz = curdyn->d_un.d_val / SIZEOF_VOID_P;
                break;
            case DT_INIT_ARRAY:
                f->init_array = (init_func*)(curdyn->d_un.d_ptr + f->offset);
                break;
            case DT_INIT_ARRAYSZ:
                f->init_arraysz = curdyn->d_un.d_val / SIZEOF_VOID_P;
                break;
            case DT_INIT:
                f->init_func = (init_func)(curdyn->d_un.d_ptr + f->offset);
                break;
        }
    }

    /* load in dependencies */
    for (curdyn = f->dynamic; curdyn && curdyn->d_tag != DT_NULL; curdyn++) {
        if (curdyn->d_tag == DT_NEEDED) {
            loadELF(f->strtab + curdyn->d_un.d_val, instdir, 0);
        }
    }

    relocateELF(f);

    return f;
}

void relocateELF(struct ELF_File *f)
{
    /* do processor-specific relocation */
#define REL_P ((ssize_t) (currel->r_offset + f->offset))
#define REL_S ((ssize_t) (findELFSymbol( \
                f->strtab + f->symtab[ELF_R_SYM(currel->r_info)].st_name, \
                NULL, f, NULL, NULL)))
#ifdef USE_RELA
#define REL_A (currel->r_addend)
#else
#define REL_A (*((ssize_t *) REL_P))
#endif

#define WORD16_REL(to) (*((int16_t *) REL_P) = (int16_t) (to))
#define WORD32_REL(to) (*((int32_t *) REL_P) = (int32_t) (to))
#define WORD64_REL(to) (*((int64_t *) REL_P) = (int64_t) (to))
#define WORD_REL(to)   (*((ssize_t *) REL_P) = (ssize_t) (to)) // Generic

    /* we ought to have rel and symtab defined */
    if (f->rel && f->symtab) {
        REL_TYPE *currel = f->rel;
        for (; currel < f->rel + f->relsz; currel += f->relent) {
            switch (ELF_R_TYPE(currel->r_info)) {
                // Switch structure heavily inspired by ASOP's bionic.
                case R_GENERIC_GLOB_DAT:
                    WORD_REL(REL_S + REL_A);
                    break;
                case R_GENERIC_RELATIVE:
                    WORD_REL((uintptr_t)f->loc + REL_A);
                    break;
                case R_GENERIC_IRELATIVE: { // This... is actually a thing. <_<
                    typedef uintptr_t (*ifunc_resolver_t)(void);
                    ifunc_resolver_t ifunc_resolver = (ifunc_resolver_t)REL_P;
                    WORD_REL(ifunc_resolver());
                }   break;
                default:
                    fprintf(stderr, "Unsupported relocation %d in %s\n", (int) ELF_R_TYPE(currel->r_info), f->nm);
                case R_GENERIC_NONE:
                    break;
#if defined(__x86_64__)
                case R_X86_64_64:
                    WORD64_REL(REL_S + REL_A);
                    break;
                case R_X86_64_32:
                    WORD32_REL(REL_S + REL_A);
                    break;
                case R_X86_64_PC32:
                    WORD32_REL(REL_S + REL_A - REL_P);
                    break;
                case R_X86_64_COPY: {
                    /* this is a bit more convoluted, as we need to find it in both places and copy */
                    Elf_Sym *localsym, *sosym;
                    localsym = &(f->symtab[ELF_R_SYM(currel->r_info)]);
                    void *soptr = findELFSymbol(
                            f->strtab + localsym->st_name,
                            NULL, NULL, f, &sosym);

                    /* OK, we should have both, so copy it over */
                    if (localsym && sosym) {
                        memcpy((void *) (localsym->st_value + f->offset),
                               soptr, sosym->st_size);
                    } else {
                        /* depend on localsym's size */
                        memcpy((void *) (localsym->st_value + f->offset),
                               soptr, localsym->st_size);
                    }
                }   break;
#elif defined(__i386__)
                case R_386_32:
                    WORD32_REL(REL_S + REL_A);
                    break;
                case R_386_PC32:
                    WORD32_REL(REL_S + REL_A - REL_P);
                    break;
#elif defined(__arm__)
                case R_ARM_ABS32:
                    WORD32_REL(REL_S + REL_A);
                    break;
                case R_ARM_REL32:
                    WORD32_REL(REL_S + REL_A - REL_P);
                    break;
#elif defined(__aarch64__)
                case R_AARCH64_ABS64:
                    WORD64_REL(REL_S + REL_A);
                    break;
                case R_AARCH64_ABS32: {
                    // TODO: This pattern is happening multiple (too many?) times, deduplicate w/ macro?
                    // Macro tokens can't be pasted together with a macro and expanded once again.
                    // Ex. with INT32_MIN defined, I can't "CONCAT(CONCAT(INT, 32), _MIN)"
                    // and expect it to expand to the value of INT32_MIN.
                    const Elf_Addr min32 = INT32_MIN;
                    const Elf_Addr max32 = UINT32_MAX;
                    uint32_t val = REL_S + REL_A;
                    if ((min32 > val) || (val > max32)) {
                        printf("Warning: ABS32 0x%016llx out of range 0x%016llx to 0x%016llx\n",
                                val, min32, max32);
                    }
                    WORD32_REL(val);
                }   break;
                case R_AARCH64_ABS16: {
                    const Elf_Addr min16 = INT16_MIN;
                    const Elf_Addr max16 = UINT16_MAX;
                    uint32_t val = REL_S + REL_A;
                    if ((min16 > val) || (val > max16)) {
                        printf("Warning: ABS16 0x%016llx out of range 0x%016llx to 0x%016llx\n",
                                val, min16, max16);
                    }
                    WORD16_REL(val);
                }   break;
                case R_AARCH64_PREL64:
                    WORD64_REL(REL_S + REL_A - REL_P);
                    break;
                case R_AARCH64_PREL32: {
                    const Elf_Addr min32 = INT32_MIN;
                    const Elf_Addr max32 = UINT32_MAX;
                    uint32_t val = REL_S + REL_A - REL_P;
                    if ((min32 > val) || (val > max32)) {
                        printf("Warning: PREL32 0x%016llx out of range 0x%016llx to 0x%016llx\n",
                                val, min32, max32);
                    }
                    WORD32_REL(val);
                }   break;
                case R_AARCH64_PREL16: {
                    const Elf_Addr min16 = INT16_MIN;
                    const Elf_Addr max16 = UINT16_MAX;
                    uint32_t val = REL_S + REL_A - REL_P;
                    if ((min16 > val) || (val > max16)) {
                        printf("Warning: PREL16 0x%016llx out of range 0x%016llx to 0x%016llx\n",
                                val, min16, max16);
                    }
                    WORD32_REL(val);
                }   break;
#endif
            }
        }
    }

    if (f->jmprel && f->symtab) {
        REL_TYPE *currel = (REL_TYPE *) f->jmprel;
        for (; (uintptr_t)currel < ((uintptr_t)f->jmprel + f->jmprelsz); currel += f->relent) {
            switch (ELF_R_TYPE(currel->r_info)) {
                case R_GENERIC_JUMP_SLOT:
                    WORD_REL(REL_S);
                    break;

                default:
                    fprintf(stderr, "Unsupported jmprel relocation %d in %s\n", (int) ELF_R_TYPE(currel->r_info), f->nm);
            }
        }
    }
}

/* Initialize the ELF passed to the function. */
void initELF(struct ELF_File *f) {
    size_t i;

    if (f == NULL) return;

    gelfload_flush_dcache();
    gelfload_invalidate_icache();

    if (f->preinit_arraysz && f->preinit_array) {
        for (i = 0;i < f->preinit_arraysz; ++i) {
            if (f->preinit_array[i] == NULL) continue;
            ((init_func)f->preinit_array[i])();
        }
    }
    if (f->init_arraysz && f->init_array) {
        for (i = 0;i < f->init_arraysz; ++i) {
            if (f->init_array[i] == NULL) continue;
            ((init_func)f->init_array[i])();
        }
    }
    if (f->init_func) {
        f->init_func();
    }
}

/* Initialize every ELF loaded /except/ for f (usually the binary)
 * Use of this function should not be mixed with the above function.
 * Unless, in the future, a flag is created to mark initialized files. */
void initELFs(struct ELF_File *except)
{
    ssize_t i;
    struct ELF_File *f;

    gelfload_flush_dcache();
    gelfload_invalidate_icache();

    // TODO: Why am I counting down instead of up?
    for (i = elfFileCount - 1; i >= 0; --i) {
        f = &(elfFiles[i]);
        if (f == except) continue;

        if (f->preinit_arraysz && f->preinit_array) {
            for (i = 0;i < f->preinit_arraysz; ++i) {
                if (f->preinit_array[i] == NULL) continue;
                ((init_func)f->preinit_array[i])();
            }
        }
    } // a little bit of code duplication, but less than the other method.

    for (i = elfFileCount - 1; i >= 0; i--, f = &(elfFiles[i])) {
        if (f == except) continue;

        if (f->init_arraysz && f->init_array) {
            for (i = 0;i < f->init_arraysz; ++i) {
                if (f->init_array[i] == NULL) continue;
                ((init_func)f->init_array[i])();
            }
        }
    }

    for (i = elfFileCount - 1; i >= 0; i--, f = &(elfFiles[i])) {
        if (f == except) continue;

        if (f->init_func) f->init_func();
    }
}

void *elfload_hostlib_sym(const char *nm, struct ELF_File *f) {
    char lsym[1024];
    void *hostsym;
    snprintf(lsym, 1024, "gelfload__%s", nm);

#if defined(HAVE_DLFCN_H) // move into host mapping func.
    hostsym = dlsym(f->prog, lsym);
    if (hostsym) return hostsym;
    hostsym = dlsym(f->prog, nm);
    if (hostsym) return hostsym;
    return NULL;
#elif defined(__WIN32)
    char csym[1024];
    int isimp = 0;

    /* Remove _imp__ if it's present */
    if (strncmp(nm, "_imp__", 6) == 0) {
        isimp = 1;
        nm += 6;
        snprintf(lsym, 1024, "gelfload__%s", nm);
    }

    /* Try adding a _ first, to get the cdecl version */
    snprintf(csym, 1024, "_%s", lsym);
    hostsym = GetProcAddress(f->prog, csym);
    if (hostsym == NULL)
        hostsym = GetProcAddress(f->prog, lsym);
    if (hostsym == NULL) {
        snprintf(csym, 1024, "_%s", nm);
        hostsym = GetProcAddress(f->proc, csym);
    }
    if (hostsym == NULL)
        hostsym = GetProcAddress(f->prog, nm);
    if (hostsym) {
        if (isimp) {
            /* Need a pointer to this pointer */
            void **pptr = (void **)gelfload_malloc(sizeof(void *));
            if (pptr == NULL) {
                perror("malloc");
                exit(1);
            }
            *pptr = hostsym;
            return (void *) pptr;

        } else {
            return hostsym;

        }
    }
#endif
    return NULL;
}

/* Find a symbol within the currently loaded ELF files
 * localin: Pointer to the current file, where STB_LOCAL symbols are OK.
 * notin: Do not bind to symbols in this file.
 * Either can be -1 */
// TODO: make this less confusing to use.
// TODO: this doesn't need to be external, does it?
void *findELFSymbol(const char *nm, const struct ELF_File *onlyin, const struct ELF_File *localin, const struct ELF_File *notin, Elf_Sym **syminto)
{
    int i;
    struct ELF_File *f;
    Elf_Word hash = sysv_hash((unsigned char *) nm);
    Elf_Word bucket, index;
    Elf_Sym *sym;
    void *hostsym;
    if (syminto) *syminto = NULL;

    if (nm[0] == '\0') return NULL;

    for (i = 0; i < elfFileCount; ++i) {
        f = &(elfFiles[i]);

        if (f == notin) continue;
        if (onlyin && f != onlyin) continue;

        /* if this is a host library, just try the host method */
        if (f->hostlib) {
            hostsym = f->hostlib(nm, f);
            if (hostsym) return hostsym;
            continue;
        }

        /* figure out the bucket ... */
        bucket = hash % ELFFILE_NBUCKET(f);

        /* then find the chain entry */
        index = ELFFILE_BUCKET(f, bucket);

        /* and work our way through the chain */
        for (; index != STN_UNDEF; index = ELFFILE_CHAIN(f, index)) {
            sym = &(f->symtab[index]);

            /* see if it's defined */
            if (strcmp(f->strtab + sym->st_name, nm) == 0 &&
                (f == localin || ELF_ST_BIND(sym->st_info) != STB_LOCAL) &&
                sym->st_shndx != SHN_UNDEF) {
                /* we found our symbol! */
                if (syminto != NULL) {
                    *syminto = sym;
                }
                return (void *) (sym->st_value + f->offset);
            }
        }
    }

    /* uh oh, not found! */
    fprintf(stderr, "Symbol undefined: '%s'\n", nm);
    return NULL;
}

/* A handy function to read a file or mmap it, as appropriate */
void readFile(const char *nm, const char *instdir, struct ELF_File *ef)
{
    /* try with instdir */
    char *longnm = (char *)gelfload_malloc(strlen(nm) + strlen(instdir) + 18);
    if (longnm == NULL) {
        perror("malloc");
        exit(1);
    }
    sprintf(longnm, "%s/../lib/gelfload/%s", instdir, nm);

#ifdef HAVE_MMAP
{
    void *buf;
    struct stat sbuf;
    int fd;

    /* use mmap. First, open the file and get its length */
    fd = open(nm, O_RDONLY);
    if (fd == -1) {
        fd = open(longnm, O_RDONLY);

        if (fd == -1) {
            perror(nm);
            exit(1);
        }
    }
    gelfload_free(longnm);
    if (fstat(fd, &sbuf) < 0) {
        perror(nm);
        exit(1);
    }

    /* then mmap it */
    buf = mmap(NULL, sbuf.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
    if (buf == NULL) {
        perror("mmap");
        exit(1);
    }

    close(fd);

    /* and put it in ef */
    ef->prog = buf;
    ef->proglen = sbuf.st_size;
}
#else
{
    char *buf;
    int bufsz, rdtotal, rd;
    FILE *f;

    /* OK, use stdio */
    f = fopen(nm, "rb");
    if (f == NULL) {
        f = fopen(longnm, "rb");

        if (f == NULL) {
            perror(nm);
            exit(1);
        }
    }
    gelfload_free(longnm);

    /* start with a 512-byte buffer */
    bufsz = 512;
    buf = (char *)gelfload_malloc(bufsz);
    if (buf == NULL) {
        perror("malloc");
        exit(1);
    }

    /* and read in the file */
    rdtotal = 0;
    while ((rd = fread(buf + rdtotal, 1, bufsz - rdtotal, f)) != 0) {
        rdtotal += rd;
        if (rdtotal != bufsz) {
            /* done reading */
            break;

        } else {
            bufsz <<= 1;
            buf = (char *)realloc(buf, bufsz);
            if (buf == NULL) {
                perror("realloc");
                exit(1);
            }
        }
    }
    if (ferror(f)) {
        perror(nm);
        exit(1);
    }
    fclose(f);

    /* now put it in ef */
    ef->prog = buf;
    ef->proglen = rdtotal;
}
#endif
}

/* The finalization function for readFile */
void closeFile(struct ELF_File *ef)
{
#ifdef HAVE_MMAP
    munmap(ef->prog, ef->proglen);
#else
    gelfload_free(ef->prog);
#endif
}
