#pragma once
#include "elfload.h"

void readFile(const char *nm, const char *instdir, struct ELF_File *ef);
void closeFile(struct ELF_File *ef);

void *findELFSymbol(const char *nm, const struct ELF_File *onlyin,
                    const struct ELF_File *localin, const struct ELF_File *notin,
                    Elf_Sym **syminto);
