#pragma once

#ifndef GELFLOAD_MEMORY_ONCE
#define GELFLOAD_MEMORY_ONCE

#include <stddef.h>

/* Used instead of malloc, so that the user can provide it. May be defined to malloc. */
void *gelfload_malloc(size_t size);
void gelfload_free(void *ptr);

/* Allocates memory based on the platform's page size. Automatically rounds up.
   Previously bbuffer(). */
void *gelfload_pagealloc(void *addr, size_t size);

/* Reprotects pages. Automatically rounds up. */
int gelfload_reprotect(void *addr, size_t size, int prot);

/* Suite of invalidation/flushing functions.
 * Defined to an empty function by default, overridden as necessary. */
void gelfload_invalidate_icache(void);
void gelfload_flush_dcache(void);

#endif /* end of include guard: GELFLOAD_MEMORY_ONCE */
