#include "elf_common.h"

/* The standard ELF hash function. */
uint32_t sysv_hash(const unsigned char *name) {
    uint_fast32_t h = 0, g;

    while (*name) {
        h = (h << 4) + *name++;
        if ((g = h & 0xf0000000))
            h ^= g >> 24;
        h &= ~g;
    }
    return h;
}

/* Non-standard GNU hash function, taken from musl libc linker. */
uint32_t gnu_hash(const unsigned char *name) {
	uint_fast32_t h = 5381;
	for (; *name; name++)
		h += h*32 + *name;
	return h;
}

// TODO: Move lookup into here as well.
