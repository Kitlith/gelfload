#include <stdlib.h>
#include <string.h>

#include "internal.h"
#include "elfload_dlfcn.h"

const char *elfload_dlinstdir = NULL;
static const char *dlLastError = NULL;

void *elfload_dlopen(const char *filename, int flag)
{
    /* pretty simple, just load based on the file name */
    struct ELF_File *f = loadELF(filename, elfload_dlinstdir, 0);
    if (f == NULL) {
        dlLastError = "Could not find or load file.";
        return NULL;
    }

    initELF(f);

    return (void *) f;
}

const char *elfload_dlerror(void) { return dlLastError; }

void *elfload_dlsym(void *handle, const char *symbol)
{
    void *sym = findELFSymbol(symbol, (struct ELF_File *)handle, NULL, NULL, NULL);

    if (sym == NULL) {
        dlLastError = "Symbol undefined.";
    }

    return sym;
}

int elfload_dlclose(void *handle) {return 0;}


void *elfload_dl(const char *fname, struct ELF_File *f)
{
    if (strcmp(fname, "dlopen") == 0) {
        return (void *) elfload_dlopen;

    } else if (strcmp(fname, "dlerror") == 0) {
        return (void *) elfload_dlerror;

    } else if (strcmp(fname, "dlsym") == 0) {
        return (void *) elfload_dlsym;

    } else if (strcmp(fname, "dlclose") == 0) {
        return (void *) elfload_dlclose;

    }

    return NULL;
}
